import React from 'react';

const ValidatorComponent = (props) => {
    let validatorMessage = null;
    if (props.length <= 5) {
        validatorMessage = "Text is too short";
    } else {
        validatorMessage = "Text is long enough";
    }

    return (
             <p>{validatorMessage}</p>
     )

}

export default ValidatorComponent; 