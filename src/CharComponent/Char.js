import React from 'react';

const CharComponent = (props) => {
    const Style={
        display:'inline-block',
        padding:16,
        textAlign:'center',
        margin:"16px",
        border:"1px solid black"
     };

    return (
             <p onClick={props.click} style={Style}>{props.text}</p>
     )

}

export default CharComponent; 