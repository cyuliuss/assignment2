import React, { Component } from 'react';
import './App.css';
import ValidatorComponent from './ValidatorComponent/Validator';
import CharComponent from './CharComponent/Char';

class App extends Component {
  state = {
    textLength: 0,
    statement: 'this is the test text for text assignment'
  }

  checkTextLength = (event) => {
    const TextString = event.target.value;

    this.setState({
      textLength: TextString.length
    })
  }

  deleteCharHandler = (word) => {
    let statement = this.state.statement
    let statementArray = statement.split(' ');
    const statementFinal = statementArray.map((wordStatement) => {
      return (wordStatement !== word) ? wordStatement : null;
    })
    // console.log(statementIndex);


    const statementText = statementFinal.join(' ');
    this.setState({
      statement: statementText
    })
  }

  render() {
    let statement = null;
    let statementArray = this.state.statement.split(' ');
    statement = (
      statementArray.map((word) => {
        if (word !== "") {
          return <CharComponent
            text={word}
            click={() => this.deleteCharHandler(word)} />
        }
      })
    );

    return (
      <div className="App">
        <p>
          Paragraph <input type="text" onChange={this.checkTextLength} />
        </p>
        <p>
          <ValidatorComponent length={this.state.textLength} />
        </p>
        <p>
          <b>{this.state.statement}</b>
        </p>
        <p>
          {statement}
        </p>
      </div>
    );
  }
}

export default App;
